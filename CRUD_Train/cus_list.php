<?php
include_once 'config/db.php';
include_once 'obj/customer.php';

//create db and objects
$database = new DB();
$db = $database->getConnection();
$customer = new Customer($db);

//call read data customers
$stmt = $customer->summonAll();
$rCount = $stmt->rowCount();
#$rowResult = $stmt->fetchAll(PDO::FETCH_ASSOC);

	
?>

<html>
	<head>
		<h1>
			LIST CUSTOMER
		</h1>
	</head>
	<body>
		<?php 
		if ($rCount>0){
			echo "<table border='1'>
					<tr>
						<td><a href='cus_add.php'>Create New</a></td>
					</tr>
					<tr></tr>
					<tr>
						<th>Account No</th>
						<th>Company Name</th>
						<th>P.I.C</th>
						<th>Phone Number</th>
						<th style='border-right: none' width='10%''></th>
						<th style='border-right: none; border-left: none'>Action</th>
						<th style='border-left: none' width='10%'></th>
					</tr>";
			#extracting row from db
			#foreach ($rowResult as $record) {
			#	echo "<tr>";
			#	foreach ($record as $key => $value) {
			#		echo "<td>".$value."</td>";
			#	}
			#	echo "</tr>";
			#}
			while ($rowResult = $stmt->fetch(PDO::FETCH_ASSOC)) {
				extract($rowResult);
				echo "<tr>
					  <td>{$acc_no}</td>
					  <td>{$cname}</td>
					  <td>{$attn}</td>
					  <td>{$contact}</td>
					  <td><a href='cus_dtl.php?cid={$cid}&edt=no'>Details</a></td>
					  <td><a href='cus_dtl.php?cid={$cid}&edt=yes'>Edit</a></td>
					  <td><a href='cus_del.php?cid={$cid}' OnClick=\"return confirm('Are you sure? Data will be lost!')\">Delete</a></td>
					  </tr>";

			}
				"</table>";
		}
		
		?>
	</body>
</html>