<?php
class Customer{

	private $dbconn;
	private $tbl_name = "customer";

	//list of columns in table
	public $accno; #customer id
	public $cname; #customer/company name
	public $adr1; #address1st  line
	public $adr2;
	public $adr3;
	public $attn; #PIC of company/customer
	public $phone; #contact number
	public $dtime; #date added

	//transfers connection cred to class
	public function __construct($db){
		$this->dbconn = $db;
	}

	//function to create product
	function create(){
		#settle query
		$qr = "INSERT INTO ".$this->tbl_name."
			   SET acc_no=:accno, cname=:cname, address1=:adr1, address2=:adr2, address3=:adr3, contact=:phone, attn=:attn, date_added=:dtime";
		$stmt = $this->dbconn->prepare($qr);

		#set time value
		$this->dtime = date('Y-m-d H:i:s');

		#echo $this->accno." ".$this->cname." ".$this->adr1." ".$this->adr2." ".$this->adr3." ".$this->attn." ".$this->phone." ".$this->dtime;
		#binding values
		$stmt->bindParam(":accno",$this->accno);
		$stmt->bindParam(":cname",$this->cname);
		$stmt->bindParam(":adr1",$this->adr1);
		$stmt->bindParam(":adr2",$this->adr2);
		$stmt->bindParam(":adr3",$this->adr3);
		$stmt->bindParam(":phone",$this->phone);
		$stmt->bindParam(":attn",$this->attn);
		$stmt->bindParam(":dtime",$this->dtime);
		#try {
		#	$stmt->execute();
		#} catch (Exception $e) {
		#	echo $e->getMessage()
		#}
		if ($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}

	//function to update product
	function update(){
		#settle query
		$qr = "UPDATE ".$this->tbl_name."
			   SET cname=:cname, address1=:adr1, address2=:adr2, address3=:adr3, attn=:attn, contact=:phone 
			   WHERE acc_no=:accno";
		#prepares query
		$stmt = $this->dbconn->prepare($qr); 

		#binds values
		$stmt->bindParam(":accno",$this->accno);
		$stmt->bindParam(":cname",$this->cname);
		$stmt->bindParam(":adr1",$this->adr1);
		$stmt->bindParam(":adr2",$this->adr2);
		$stmt->bindParam(":adr3",$this->adr3);
		$stmt->bindParam(":phone",$this->phone);
		$stmt->bindParam(":attn",$this->attn);

		#execute query
		if ($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}

	//function to read product
	function summonAll(){
		#settle query
		$qr = "SELECT cid, acc_no, cname, attn, contact FROM ".$this->tbl_name." ORDER BY acc_no ASC";
		#prepares query
		$stmt = $this->dbconn->prepare($qr); 

		#execute query
		$stmt->execute();
		return $stmt;
	}

	//function to read one data
	function summonOne($cid){
		#settle query
		$qr = "SELECT * FROM ".$this->tbl_name." WHERE cid=".$cid." LIMIT 0,1";
		#prepares query
		$stmt = $this->dbconn->prepare($qr); 

		#execute query
		$stmt->execute();
		return $stmt;
	
	}

	//function to delete product
	function byebye($cid){
		$qr ="DELETE FROM ".$this->tbl_name." WHERE cid=".$cid;
		#prepares query
		$stmt = $this->dbconn->prepare($qr);

		#execute query
		if ($stmt->execute()){
			$notice = "Data has been succesfully deleted.";
		}else{
			$notice = "Data has not been deleted.";
		}
		return $notice;

	}
}

?>